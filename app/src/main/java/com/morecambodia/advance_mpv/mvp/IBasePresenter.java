package com.morecambodia.advance_mpv.mvp;

public interface IBasePresenter<ViewT> {

    void onViewActive(ViewT view);

    void onViewInactive();

    void onInitView();
}