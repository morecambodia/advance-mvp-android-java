package com.morecambodia.advance_mpv.module.login;

import android.content.Context;

import com.morecambodia.advance_mpv.mvp.IBasePresenter;
import com.morecambodia.advance_mpv.mvp.IBaseView;

public interface  LoginView {

    interface View extends IBaseView {

        void showSuccess(String message);

        void showError(String message);
    }

    interface Presenter extends IBasePresenter<View> {
        @Override
        void onInitView();

        void onLogin(Context context, String username, String password);
    }
}
