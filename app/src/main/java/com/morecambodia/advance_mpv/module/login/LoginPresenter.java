package com.morecambodia.advance_mpv.module.login;

import android.content.Context;

import com.morecambodia.advance_mpv.mvp.BasePresenter;

public class LoginPresenter extends BasePresenter<LoginView.View> implements LoginView.Presenter {
    public LoginPresenter(LoginView.View view){
        this.view = view;
    }
    @Override
    public void onLogin(Context context, String username, String password) {
        String userNameStore = "mvp@gmail.com";
        String passwordStore = "mvp123456";
        if(!userNameStore.equals(username)){
            this.view.showError("Your email invalid");
            return;
        }
        if(!passwordStore.equals(password)){
            this.view.showError("Your password invalid");
            return;
        }
        this.view.showSuccess("Presenter call API success and tell view show message");
    }

    @Override
    public void onInitView() {
        this.view.initView();
    }
}
